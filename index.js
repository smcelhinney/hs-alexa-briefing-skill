module.change_code = 1;
'use strict';

const alexa = require('alexa-app');
const app = new alexa.app('daily-briefing');
const moment = require('moment');

app.launch((request, response) => {
  response.say('Welcome to your test skill')
    .reprompt('Way to go. You got it to run. Bad ass.')
    .shouldEndSession(false);
});


app.error = (exception, request, response) => {
  console.log(exception)
  console.log(request);
  console.log(response);
  response.say(`Sorry an error occured:  ${error.message}`);
};

app.intent('GetDailyBriefing', {
    'slots': {
      'briefingDate': 'AMAZON.DATE'
    },
    'utterances': [
      'whats my HubSpot briefing',
      'whats my HubSpot briefing for {-|briefingDate}'
    ]
  },
  (request, response) => {
    const briefingDate = request.slot('briefingDate') || moment().format('YYYY-MM-dd');
    console.log(briefingDate);
    response.say(`You asked for the briefing for ${briefingDate}`);
  }
);

module.exports = app;
